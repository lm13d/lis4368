# LIS 4368

## Lindsey Mabrey

### Assignments:

[A1 Subdirectory](lis4368a1/README.md "Assignment 1 course work")

[A2 Subdirectory](lis4368a2/README.md "Assignment 2 course work")

[A3 Subdirectory](lis4368a3/READMEa3.md "Assignment 3 course work")

[P1 Subdirectory](p1/README.md "Project 1 course work")

[A5 Subdirectory](lis4368a5/README.md "Assignment 5 course work")

[P2 Subdirectory](lis4368p2/README.md "Project 2 course work")