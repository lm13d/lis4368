> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4368

## Lindsey Mabrey

### Assignment 5 Requirements:

*Sub-Heading:*

1. Prepared statements to help prevent SQL injection
2. JSTL to prevent XSS
3. Adds insert functionality to A4

#### README.md file should include the following items:

* Screenshot of pre-form validation
* Screenshot of form validation
* Screenshot of post-valid user form entry


#### Assignment 5 Screenshots:

*Screenshot of running Form Validation*:

![Pre-form validation](img/database.png)
![Form validation](img/formvalidation.png)
![Post-form validation](img/thanks.png)

